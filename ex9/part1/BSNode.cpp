#include "BSNode.h"

BSNode::BSNode(std::string data)
{
	this->_data = data;
	this->_count = 1;
	this->_left = nullptr;
	this->_right = nullptr;
}

BSNode::BSNode(const BSNode& other)
{
	*this = other;//operator=
}

BSNode::~BSNode()
{
	if (this != nullptr)
	{
		if (this->_left != nullptr)
		{
			delete this->_left;
		}
		if (this->_right != nullptr)
		{
			delete this->_right;
		}
		this->_left = nullptr;
		this->_right = nullptr;
	}
}

void BSNode::insert(std::string value)
{
	if (this->search(value))
	{
		this->_count++;
	}
	//if the value is smaller than this data need to put the value in the left place
	if (value < this->_data)
	{
		if (this->_left == nullptr)
		{
			this->_left = new BSNode(value);
		}
		else
		{
			this->_left->insert(value);//need to go to the next 
		}
	}
	//if the value is bigger than this data need to put the value in the right place 
	else if (value > this->_data)
	{
		if (this->_right == nullptr)
		{
			this->_right = new BSNode(value);
		}
		else
		{
			this->_right->insert(value);//need to go to the next 
		}
	}
}

BSNode& BSNode::operator=(const BSNode& other)
{
	if (this == nullptr)
	{
		return *this;
	}
	else
	{
		this->_data = other._data;
		this->_count = other._count;

		if (other._left != nullptr)
		{
			this->_left = new BSNode(*other._left);
		}
		else if (other._left == nullptr)
		{
			this->_left = nullptr;
		}
		
		if (other._right != nullptr)
		{
			this->_right = new BSNode(*other._right);
		}
		else if (other._right == nullptr)
		{
			this->_right = nullptr;
		}
	}
	
}

bool BSNode::isLeaf() const
{
	if (this->_left == nullptr && this->_right == nullptr)
	{
		return true;
	}
	else
	{
		return false;
	}
}

std::string BSNode::getData() const
{
	return this->_data;
}

BSNode* BSNode::getLeft() const
{
	return this->_left;
}

BSNode* BSNode::getRight() const
{
	return this->_right;
}

bool BSNode::search(std::string val) const
{
	//if the val is found it will return true
	if (this->_data == val)
	{
		return true;
	}
	else
	{
		if (this->_left != nullptr)
		{
			if (this->_left->search(val) == true)
			{
				return true;
			}
		}
		if (this->_right != nullptr)
		{			
			if (this->_right->search(val))
			{
				return true;
			}
		}
	}
	return false;
}

int BSNode::getHeight() const
{
	if (this->isLeaf())
	{
		return 0;
	}
	else
	{
		int r_h = this->_right->getHeight();
		int l_h = this->_left->getHeight();

		return 1 + std::max(r_h, l_h);
	}
}

int BSNode::getDepth(const BSNode& root) const
{
	if (!root.search(this->_data))//if the data not found need to return -1
	{
		return -1;
	}
	return this->getCurrNodeDistFromInputNode(&root);

}

void BSNode::printNodes() const
{
	if (this == NULL)
		return;

	this->_left->printNodes();
	std::cout << this->_data << " ";
	this->_right->printNodes();
}

int BSNode::getCurrNodeDistFromInputNode(const BSNode* node) const
{
	if (node == nullptr || this == node)
	{
		return 0;
	}
	else
	{
		if (this->_data < node->_data)
		{
			return 1 + this->getCurrNodeDistFromInputNode(node->_left);
		}
		else
		{
			return 1 + this->getCurrNodeDistFromInputNode(node->_right);
		}
	}
}


