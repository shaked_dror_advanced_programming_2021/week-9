#pragma once
#include <iostream>
#include <string>

template <class T>
class BSNode
{
public:

#include "BSNode.h"

	BSNode(T data)
	{
		this->_data = data;
		this->_count = 1;
		this->_left = nullptr;
		this->_right = nullptr;
	}

	BSNode(const BSNode& other)
	{
		*this = other;//operator=
	}

	~BSNode()
	{
		if (this != nullptr)
		{
			if (this->_left != nullptr)
			{
				delete this->_left;
			}
			if (this->_right != nullptr)
			{
				delete this->_right;
			}
			this->_left = nullptr;
			this->_right = nullptr;
		}
	}

	void insert(T value)
	{
		if (this->search(value))
		{
			this->_count++;
		}
		//if the value is smaller than this data need to put the value in the left place
		if (value < this->_data)
		{
			if (this->_left == nullptr)
			{
				this->_left = new BSNode(value);
			}
			else
			{
				this->_left->insert(value);//need to go to the next 
			}
		}
		//if the value is bigger than this data need to put the value in the right place 
		else if (value > this->_data)
		{
			if (this->_right == nullptr)
			{
				this->_right = new BSNode(value);
			}
			else
			{
				this->_right->insert(value);//need to go to the next 
			}
		}
	}

	BSNode& operator=(const BSNode& other)
	{
		if (this == nullptr)
		{
			return *this;
		}
		else
		{
			this->_data = other._data;
			this->_count = other._count;

			if (other._left != nullptr)
			{
				this->_left = new BSNode(*other._left);
			}
			else if (other._left == nullptr)
			{
				this->_left = nullptr;
			}

			if (other._right != nullptr)
			{
				this->_right = new BSNode(*other._right);
			}
			else if (other._right == nullptr)
			{
				this->_right = nullptr;
			}
		}

	}

	bool isLeaf() const
	{
		if (this->_left == nullptr && this->_right == nullptr)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	T getData() const
	{
		return this->_data;
	}

	BSNode* getLeft() const
	{
		return this->_left;
	}

	BSNode* getRight() const
	{
		return this->_right;
	}

	bool search(T val) const
	{
		//if the val is found it will return true
		if (this->_data == val)
		{
			return true;
		}
		else
		{
			if (this->_left != nullptr)
			{
				if (this->_left->search(val) == true)
				{
					return true;
				}
			}
			if (this->_right != nullptr)
			{
				if (this->_right->search(val))
				{
					return true;
				}
			}
		}
		return false;
	}

	int getHeight() const
	{
		if (this->isLeaf())
		{
			return 0;
		}
		else
		{
			int r_h = this->_right->getHeight();
			int l_h = this->_left->getHeight();

			return 1 + std::max(r_h, l_h);
		}
	}

	int getDepth(const BSNode& root) const
	{
		if (!root.search(this->_data))//if the data not found need to return -1
		{
			return -1;
		}
		return this->getCurrNodeDistFromInputNode(&root);

	}

	void printNodes() const
	{
		if (this == NULL)
			return;

		this->_left->printNodes();
		std::cout << this->_data << " ";
		this->_right->printNodes();
	}

private:
	T _data;
	BSNode* _left;
	BSNode* _right;

	int _count; //for question 1 part B
	int getCurrNodeDistFromInputNode(const BSNode* node)
	{
		if (node == nullptr || this == node)
		{
			return 0;
		}
		else
		{
			if (this->_data < node->_data)
			{
				return 1 + this->getCurrNodeDistFromInputNode(node->_left);
			}
			else
			{
				return 1 + this->getCurrNodeDistFromInputNode(node->_right);
			}
		}
	}
};
