#include <iostream>
#include "BSNode.h"
#define SIZE 15

template<class T>
void printArray(T arr[], int n);

int main()
{
	std::string str_arr[SIZE] = {"a", "c", "z", "d", "h", "hj", "oo", "op", "aa", "ds", "ft", "yu", "bv", "vb", "gg"};
	int int_arr[SIZE] = { 3,4,2,5,7,8,9,12,13,11,15,76,56,1,78 };
	::printArray(str_arr, SIZE);
	std::cout << std::endl;
	::printArray(int_arr, SIZE);

	//make the trees
	BSNode<std::string> str_tree = BSNode<std::string>(str_arr[0]);
	for (int i = 1; i < SIZE; i++)
	{
		str_tree.insert(str_arr[i]);
	}
	BSNode<int> int_tree = BSNode<int>(int_arr[0]);
	for (int i = 1; i < SIZE; i++)
	{
		int_tree.insert(int_arr[i]);
	}
	std::cout << "str: " << std::endl;
	str_tree.printNodes();
	std::cout <<std::endl;
	std::cout << "int: " << std::endl;
	int_tree.printNodes();

	return 0;
}


template<class T>
void printArray(T arr[], int n)
{
	for (int i = 0; i < n; i++)
	{
		std::cout << arr[i] << " ";
	}
}