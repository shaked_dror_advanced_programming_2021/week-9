#pragma once
#include <iostream>


template<class T>
int compare(T first, T sec)
{
	if (first < sec)
	{
		return 1;
	}
	else if (first == sec)
	{
		return 0;
	}
	else
	{
		return -1;
	}
}

template<class T>
void bubbleSort(T arr[], int n)
{
	for (int i = 0; i < n - 1; i++)
	{
		for (int j = 0; j < n - i - 1; j++)
		{
			if (arr[j] > arr[j + 1])
			{
				//swap
				T temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
			}
		}
	}
}

template<class T>
void printArray(T arr[], int n)
{
	for (int i = 0; i < n; i++)
	{
		std::cout << arr[i] << std::endl;
	}
}




