#pragma once
#include <iostream>
#include <string>
#include <iostream>

class My
{
public:
	int _member;
	My(int member);

	friend std::ostream& operator<<(std::ostream& os, const My& dt);
	bool operator<(const My other);
	bool operator>(const My other);
	bool operator==(const My other);
};