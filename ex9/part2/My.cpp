#include "My.h"

My::My(int member) 
{
	this->_member = member;
}

bool My::operator<(const My other)
{
	if (this->_member < other._member)
	{
		return true;
	}
	return false;
}

bool My::operator>(const My other)
{
	if (this->_member > other._member)
	{
		return true;
	}
	return false;
}

bool My::operator==(const My other)
{
	if (this->_member == other._member)
	{
		return true;
	}
	return false;
}

std::ostream& operator<<(std::ostream& os, const My& my)
{
	os << my._member;
	return os;
}
